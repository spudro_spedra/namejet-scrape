from bs4 import BeautifulSoup
from nltk.corpus import words
import urllib.request, csv, os.path, datetime

url = "http://www.namejet.com/Pages/Downloads.aspx?fbclid=IwAR3IsCoaqrHObA5fsWJnhGE73i07oqiSpTUWnJJmoe2lv9siClKHo2k9mmU"
path_of_folder = os.path.dirname(os.path.realpath(__file__)) + '/data/'

def get_html(url):
	headers = {'User-Agent': 'Mozilla/5.0 (WINDOWS NT 6.1; Win64; x64; rv:47.0)'}

	req = urllib.request.Request(url, headers=headers)

	try:
		with urllib.request.urlopen(req) as binary_file:
			site_html = binary_file.read().decode('utf-8')
			return site_html

	except Exception as e:
		print("Something went wrong:")
		print(str(e))
		exit(0)

def get_links(soup):

	links = []

	for a_tag in soup.find_all('a'):
		#links.append(link.li['id'])
		try:
			link = a_tag.get('href')
			if (link is not None) and ((".txt" in link) or (".csv" in link)) and ("prerelease_" not in link):
				links.append(link)
		except Exception as e:
			print("{0} {1}".format("Exception: ", e))
			pass

	return links

def download_all_links(links):

	print("Downloading...")

	link_url = 'http://www.namejet.com'

	for link in links:
		complete_url = ""
		complete_url += link_url + link

		urllib.request.urlretrieve(complete_url, (path_of_folder + link.replace('/download/', '')))

	print("Downloaded.")

def make_folder():

	if not os.path.isdir(path_of_folder):
		os.mkdir(path_of_folder)

def return_sites_from_csv(file):

	records = []

	with open("data/{0}".format(file), newline='') as csv_file:
		reader = csv.reader(csv_file)
		for row in reader:
			for entry in row:
				if not is_date(entry) and not has_char('$', entry) and has_char('.com', entry) and not has_char('-', entry) and not has_digit(entry): # and not has_digit(entry)
					records.append(entry)

	return records

def has_char(char, string):
	return (char in string)

def is_date(string):

	try:
		datetime.datetime.strptime(string, '%Y-%m-%d')
	except ValueError:
		return False

	return True

def has_digit(site):
	return any([i.isdigit() for i in site])

def is_site_a_one_word(site):

	site_to_check = site.replace(".com", '')
	if site_to_check in words.words():
		print("FOUND: {0}".format(site))
		return site

	return False

def make_filtered_site_list():

	print("Filtering...")

	sites = []

	for file in os.listdir(path_of_folder):
		unfiltered_sites = return_sites_from_csv(file)

		for un_site in unfiltered_sites:
			if is_site_a_one_word(un_site):
				sites.append(un_site)

	print("Filtered.")

	return sites


def debug_search_for_words(site_list):
	for site in site_list:
		if is_site_a_one_word(site):
			print("FOUND: {0}".format(site))


def write_sites_file(sites):

	print("Writing file...")

	with open("Filtered search.txt", 'w') as file:
		for site in sites:
			file.write(site + "\n")

		file.close()

	print("File written.")

def main():

	html = get_html(url)

	soup = BeautifulSoup(html, 'html.parser')

	links = get_links(soup)

	make_folder()

	download_all_links(links)

	sites = make_filtered_site_list()

	write_sites_file(sites)




if __name__ == '__main__':
	main()


# napisz mi w pythonie program, ktory sciagnie listy z tej strony: http://www.namejet.com/Pages/Downloads.aspx V done
# i wyfiltruje domeny z tych list ktore sa pojdeynczymi slowami angielskimi i koncza sie .com

